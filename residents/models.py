from datetime import datetime
from residents import db


class SubscriberModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=datetime.now)
    first_name = db.Column(db.String(64), index=True, unique=True)
    last_name = db.Column(db.String(64), index=True, unique=True)
    phone_number = db.Column(db.String(32), index=True, unique=True)
    email = db.Column(db.String(128), index=True, unique=True)

    def __init__(self, first_name, last_name, phone_number, email=None):
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.email = email

    def add(self):
        db.session.add(self)
        db.session.commit()

    def dict(self):
        result = dict()
        # result["id"] = {}.format(self.id)
        result["date_created"] = "{}".format(self.date_created)
        result["first_name"] = "{}".format(self.first_name)
        result["last_name"] = "{}".format(self.last_name)
        result["phone_number"] = "{}".format(self.phone_number)
        result["email"] = "{}".format(self.email)
        return result

    def __repr__(self):
        return 'SubscriberModel(first_name={}, last_name={}, phone_number={}, email={})'.format(
            self.first_name, self.last_name, self.phone_number, self.email)

    def __str__(self):
        return str(self.dict())

