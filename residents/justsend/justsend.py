from twilio.rest import Client
from unidecode import unidecode
import threading
import queue


class JustSend:
    def __init__(self, sid, token, number):
        """
            sid - Twilio SID
            token - Twilio token
            number - Number to send from. you can set to a 8-letter ASCII message (?).
        """
        self.sid = sid
        self.token = token
        self.number = number
        self.queue = queue.Queue()

    def enqueue(self, recipient, message):
        """ Add a message to the outgoing queue. """
        self.queue.put_nowait(
            (recipient, unidecode(message).strip())
        )

    def send(self):
        """ Send messages in the queue. """
        client = Client(self.sid, self.token)

        threads = []
        for i in range(32):  # 32 threads
            t = threading.Thread(target=self.worker, args=(client,))
            threads.append(t)
            t.start()

        for t in threads:
            t.join()

    def worker(self, client):
        """ Run in threads """
        try:
            recipient, message = self.queue.get_nowait()
        except:
            return
        else:
            client.messages.create(
                to=recipient,
                from_=self.number,
                body=message
            )
