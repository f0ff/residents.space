from flask import request
from flask_restful import Resource

from residents import api
from residents.models import SubscriberModel


class Subscribers(Resource):
    # curl http://127.0.0.1:5000/subscibers
    def get(self):
        subs = SubscriberModel.query.all()
        result = dict()
        for e in subs:
            result[e.id] = e.dict()
        return {"Subscribers": result}

    # curl -H "Content-Type: application/json" -X POST -d '{"first_name": "Juliusz", "last_name": "Tarnowski",
    # "phone_number": "506249177", "email": "julusmaximus@gmail.com"}' http://127.0.0.1:5000/subscibers
    def post(self):
        some_json = request.get_json()
        sub = SubscriberModel(
            first_name=some_json['first_name'],
            last_name=some_json['last_name'],
            phone_number=some_json['phone_number'],
            email=some_json['email'])
        sub.add()
        return {'Added new': sub.dict()}, 201


api.add_resource(Subscribers, '/subscribers')
