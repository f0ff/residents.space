from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Optional, Email


class PhoneNumberField(StringField):
    def validate(self, form, extra_validators=tuple()):
        tmp = self.data.replace(" ", "")
        if not (len(tmp) == 9 and tmp.isdigit()):
            raise Exception("The number should consist of 9 numbers.")
        else:
            return True


class RegisterForm(FlaskForm):
    first_name = StringField('First name', validators=[DataRequired()])
    last_name = StringField('Last name', validators=[DataRequired()])
    phone_number = PhoneNumberField('Phone number', validators=[DataRequired()])
    mail_address = StringField('Mail address', validators=[Optional(), Email()])
    submit = SubmitField('Register')
