from residents import app, db
from flask import render_template, redirect, url_for, request
from residents.forms import RegisterForm
from residents.models import SubscriberModel


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        form = RegisterForm()
        sub = SubscriberModel(first_name=form.first_name.data,
                              last_name=form.last_name.data,
                              phone_number=form.phone_number.data,
                              email=form.mail_address.data)
        sub.add()
        return redirect(url_for('index'))
    else:
        form = RegisterForm()
        return render_template('register.html', form=form)
